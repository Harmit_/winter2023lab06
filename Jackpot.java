public class Jackpot{

	public static void main(String[] args){

	System.out.println("Welcome to Jackpot: The game!");
	Board board = new Board();
	Boolean gameOver = false;
	int numOfTilesClosed = 0;
	
	while(!gameOver){
            System.out.println(board);
            if (board.playATurn()){	
			gameOver = true;
		} else {
			numOfTilesClosed++;
			}
		}

		if (numOfTilesClosed >= 7) {
			System.out.println("We have a winner!");
		} else { 
			System.out.println("You can't win them all!");
		}
	}
}

