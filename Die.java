import java.util.Random;
public class Die {
	private int faceValue;
	private Random ran;
	
	public Die(){
		this.ran = new Random();
		this.faceValue = 1;
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		this.faceValue = ran.nextInt(6)+1;
	}
	
	public String toString(){
		return "you have: "+this.faceValue;
	}
}
