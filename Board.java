public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] tiles;

	public Board(){
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String s = "";
		for(int i=0; i < tiles.length; i++){
			if (this.tiles[i]){
				s += "X ";
				} else {
					s += (i+1) + " "; 
				}
			}
		return s;
	}
	
    public boolean playATurn()
    {
        dieOne.roll();
        dieTwo.roll();
        System.out.println(dieOne);
        System.out.println(dieTwo);
        
        int sumOfDice = dieOne.getFaceValue() + dieTwo.getFaceValue();

        if (!tiles[sumOfDice - 1]) {
            System.out.println("Closing tile equal to sum: " + sumOfDice);
            tiles[sumOfDice - 1] = true;
            return false;
        }
        else if (!(tiles[dieOne.getFaceValue()])) {
            System.out.println("Closing tile with the same value as die one: " + dieOne.getFaceValue());
            tiles[dieOne.getFaceValue()] = true;
            return false;
        } else if (!(tiles[dieTwo.getFaceValue()])) {
            System.out.println("Closing tile with the same value as die two: " + dieTwo.getFaceValue());
            tiles[dieTwo.getFaceValue()] = true;
            return false;
        } else {
            System.out.println("All the tiles for these values are already shut");
            return true;
        }
	}
}
